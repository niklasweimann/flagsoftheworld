# Flags of the World

## Usage
### Sizes:
* flag-small
* flag-small-square
* flag-normal
* flag-normal-square
* flag-big
* flag-big-square

### Round:
* flag-round


## Countries
### Africa
* Mauritius {flag-mauritius}
* Chad {flag-chad}
* Ivory Coast {flag-ivorycoast}
* Gabon {flag-gabon}
* Guinea {flag-guinea}
* Mali {flag-mali}
* Sierra Leone {flag-sierraleone}
### Asia
* Armenia {flag-armenia}
* Russia {flag-russia}
* Yemen {flag-yemen}
### Europe
* Austria {flag-austria}
* Germany {flag-germany}
* Poland {flag-poland}
* Monaco {flag-monaco}
* Netherlands {flag-netherlands}
* Bulgaria {flag-bulgaria}
* Belgium {flag-belgium}
* Estonia {flag-estonia}
* France {flag-france}
* Hungary {flag-hungary}
* Ireland {flag-ireland}
* Italy {flag-italy}
* Lithuania {flag-lithuania}
* Luxembourg {flag-luxembourg}
* Romania {flag-romania}
### South America
* Colombia {flag-colombia}
* Bolivia {flag-bolivia}
